Pentru a putea edita codul sursa este nevoie de un editor de cod care poate fi Visual Studio Code. 
Se descarca de pe site-ul oficial https://code.visualstudio.com/download.

Pentru a putea rula aplicatia este nevoie de un server Web, pentru aceasta aplicatie se foloseste apache.
Se instaleaza XAMPP cu modulele php, apache, mysql etc si se pornesc serviciile apache si mysql.
Codul sursa de la adresa mentionata anaterior se adauga in folderul htdocs creat in urma instalarii programului XAMPP.
Pentru a rula aplicatia in browser se acceseaza urmatoarea cale: http://localhost/aplicatie/Logare/Login.php, unde se pot introduce credentialele pentru logare.
